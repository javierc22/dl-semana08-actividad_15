# Ejercicio 2: Contando líneas
# Se tiene un peliculas.txt con diversas películas:
# Crear un método que lea el archivo, lo abra y devuelva la cantidad de líneas que posee.

def count_lines(filename)
  file = File.open(filename, 'r')
  data = file.readlines.map(&:chomp)
  file.close
  print "El archivo tiene #{data.count} líneas"
end

count_lines('peliculas.txt')
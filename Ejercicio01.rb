# Ejercicio 1: Escribiendo un archivo básico

# 1. Crear un método que reciba dos strings, este método creará un archivo index.html 
# y pondrá como párrafo cada uno de los strings recibidos.

# 2. Crear un método similar al anterior, que además pueda recibir un arreglo. Si el arreglo no está vacío,
# agregar debajo de los párrafos una lista ordenada con cada uno de los elementos.

# 3. Crear un tercer método que además pueda recibir un color. Agregar color de fondo a los párrafos.
# El retorno de los métodos debe devolver nil.

# ----------------------------------------------------------------------------
# 1.
a = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit 1'
b = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit 2'

def method_1(content1, content2)
  file = File.open('index.html', 'w')
  file.puts "<p> #{content1} </p>"
  file.puts "<p> #{content2} </p>"
  file.close
  nil
end

method_1(a, b)

# ----------------------------------------------------------------------------
# 2.
c = ['Hola1', 'Hola2', 'Hola3', 'Hola4', 'Hola5']

def method_2(array)
  file = File.open('index.html', 'a')
  file.puts '<lo>'
  array.each { |e| file.puts "<li> #{e} </li>" }
  file.puts '</lo>'
  file.close
  nil
end

method_2(c)

# ----------------------------------------------------------------------------
# 3.
color = '#00FF00'

def metodo_3(content1, content2, color)
  file01 = File.open('index.html', 'a')
  file01.puts "<p style= \"background: #{color}; color: white;\"> #{content1} </p>"
  file01.puts "<p style= \"background: #{color}; color: white;\"> #{content2} </p>"
  file01.close
  nil
end

metodo_3(a, b, color)

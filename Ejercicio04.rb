# -------------- EJERCICIO 4 ---------------- #

data = []
File.open('products.txt', 'r') { |file| data = file.readlines.map(&:chomp) }
data_array = [] 
data.each { |e| data_array.push(e.split(', ')) }
data_hash = {}
data_array.each { |e| data_hash[e[0].to_sym] = e[1..3].map(&:to_i) }
print data_hash

option = 0
while option != 6
  # ------------------- Main menu ------------------------- #
  puts
  puts "Escoge una opcion:\n"
  puts 'Opcion 1: Ver cantidad de productos existentes.-'
  puts 'Opcion 2: Ver Stock de un producto.-'
  puts 'Opcion 3: Ver Productos no registrados.-'
  puts 'Opcion 4: Ver existencia total menor a un valor ingresado.-'
  puts 'Opcion 5: Registrar nuevo producto.-'
  puts 'Opcion 6: Salir.-'
  puts

  option = 0
  while option <= 0 || option > 6
    puts 'Opción:'
    option = gets.chomp.to_i
    puts
  end

  # ------------------- Option 1 ------------------------- #
  def sub_menu_option_1(products)
    puts 'a. Mostrar la existencia por productos.'
    puts 'b. Mostrar la existencia total por tienda.'
    puts 'c. Mostrar la existencia total en todas las tiendas.'
    puts 'd. Volver al menú principal.'
    puts

    puts 'Opcion:'
    option = gets.chomp.to_s

    option_1(option, products)
  end

  def option_1(option, products)
    a = {}

    case option
    when 'a'
      products.each { |key, value| a[key] = value.sum }
      puts a

    when 'b'
      suma = 0
      products.each_value { |value| suma += value[0] }
      puts "Tienda 1: #{suma}"

      suma = 0
      products.each_value { |value| suma += value[1] }
      puts "Tienda 2: #{suma}"

      suma = 0
      products.each_value { |value| suma += value[2] }
      puts "Tienda 3: #{suma}"

    when 'c'
      suma = 0
      products.each_value { |value| suma += value.sum }
      puts "Total: #{suma}"
    
    when 'd'
      puts 'Volviendo al menú principal...'
    end
  end

  # ------------------- Option 2 ------------------------- #
  def stock_product(products)
    puts 'Ingrese producto a consultar:'
    search = gets.chomp
    puts "Stock total #{search}: #{ products[search.to_sym].sum }"
  end

  # ------------------- Option 3 ------------------------- #
  def unregistered_product(products)
    products.each { |key, value| puts "Tienda 1: #{key}" if value[0].zero? }
    products.each { |key, value| puts "Tienda 2: #{key}" if value[1].zero? }
    products.each { |key, value| puts "Tienda 3: #{key}" if value[2].zero? }
  end

  # ------------------- Option 4 ------------------------- #
  def see_stock(products)
    puts 'Ingrese valor:'
    valor = gets.chomp.to_i
    a = {}
    products.each { |key, value| a[key] = value.sum if value.sum < valor}
    puts 'Productos con stock menor al valor ingresado:'
    puts a
  end

  # ------------------- Option 5 ------------------------- #
  def register_product(products)
    array = []
    puts "Ingrese \'nombre_producto, stock_T1, stock_T2, stock_T3\':"
    product_new = gets.chomp.split(', ')
    array.push(product_new)
    array.each { |e| products[e[0].to_sym] = e[1..3].map(&:to_i) }
    puts "\nSe agregó un producto:"
    puts products
  end

  # ------------------- Option 6 ------------------------- #
  def exit_program
    puts 'Saliendo...'
  end

  # ------------------- Ejecutar opciones ------------------------- #
  case option
  when 1 then sub_menu_option_1(data_hash)
  when 2 then stock_product(data_hash)
  when 3 then unregistered_product(data_hash)
  when 4 then see_stock(data_hash)
  when 5 then register_product(data_hash)
  when 6 then exit_program
  end
end

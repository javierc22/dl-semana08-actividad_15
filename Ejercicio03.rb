# Ejercicio 3: Contando palabras
# 1. Crear un método que reciba el archivo peliculas.txt, lo abra y cuente la cantidad total de palabras. 
# El método debe devolver este valor.

# 2. Crear un método similar para que además reciba -como argumento- un string. 
# En este caso el método debe contar únicamente las apariciones de ese string en el archivo.

# ----------------------------------------------------------------------------
# 1.
def total_words(filename)
  data = []
  File.open( filename, 'r') { |file| data = file.read.split(' ') }
  puts "Total palabras: #{data.length}"
end

total_words('peliculas.txt')

# ----------------------------------------------------------------------------
# 2.

def search_word(filename, word)
  data = []
  File.open( filename, 'r') { |file| data = file.read.downcase.split(' ') }
  puts "La palabra #{word} aparece #{data.count(word)} vez / veces"
end

search_word('peliculas.txt', 'guerra')